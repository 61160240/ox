package oxOOP;

public class Table extends Game{
	private String[][] board;
	Player player = new Player();
	
	public Table() {
		board = new String[][]{{" ", "1", "2", "3"}, {"1", "-", "-", "-"}, {"2", "-", "-", "-"}, {"3", "-", "-", "-"}};
	}
	
	public void getBoard() {
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                System.out.print(board[row][col] + " ");
            }
            System.out.println();
        }
    }
	public String getRowCol(int row,int col) {
		return board[row][col];
	}
	public void setBoard(int row, int col,Player player) {
		if (board[row][col] == "-") {
			board[row][col] = player.getPlayer().toUpperCase();
		}else {
			player.minusTurn();
			System.out.println("--------Player "+ player.getPlayer().toUpperCase() +" already put here -------------");
			System.out.println("--------Please select new row and colmn--------");
		}
	}

}
