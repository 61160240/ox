package oxOOP;

public class Player extends Game{
	private int turn=1;
	public Player() {
		
	}
	public String getPlayer() {
		if(turn%2==1) return "o";
		return "x";
	}
	public void addTurn() {
		turn++;
	}
	public int getTurn() {
		return turn;
	}
	public void minusTurn() {
		turn--;
	}
	public String getNextPlayer() {
		if(turn%2==1) return "x";
		return "o";
	}

}
