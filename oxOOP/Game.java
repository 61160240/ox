package oxOOP;

import java.util.Scanner;

public class Game {
	private Table table;
	
	Game(){
		
	}
	public static void showWelcome() {
		System.out.println("Welcome to OX game");
	}
	public static void showTextEnd() {
		System.out.print("Good luck Bye Bye !!");
	}
	public static void showTurn(Player player) {
		System.out.print(player.getPlayer().toUpperCase());
		System.out.println(" turn");
	}
	public static void showTextInput() {
		System.out.print("Please put row and colume: ");
	}
	public static boolean isStringInteger(String x, String y) {
		try {
			Integer.parseInt(x);
			Integer.parseInt(y);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	public static boolean checkIndexOutOfBound(String row,String col) {
		if (Integer.parseInt(row) > 3 || Integer.parseInt(col) > 3) {
			System.out.println("***Please put correct row and column !!!***");
			return false;
		}return true;
	}
	public void startGame() {
		showWelcome();
		Table table = new Table();
		Player player = new Player();
		setTurn(player,table);
	}
	public static void setTurn(Player player,Table table) {
		boolean end = false;
		while (end == false) {
			table.getBoard();
			showTurn(player);
			checkInput(player,table);
			end=checkWin(table,player);
			player.addTurn();
		}
	}
	public static void checkInput(Player player,Table table) {
		String row,col;
		while (true) {
			Scanner sc = new Scanner(System.in);
			showTextInput();
			row = sc.next();
			col = sc.next();
			if (isStringInteger(row, col) == false) {
				System.out.println("****Please put correct row and column !!!****");
				continue;
			}
			if (checkIndexOutOfBound(row,col)==false) {
				continue;
			}else break;
		}
		input(player,row,col,table);
	}
	public static void input(Player player,String row,String col,Table table) {
		table.setBoard(Integer.parseInt(row), Integer.parseInt(col), player);
	}
	public static boolean checkWin(Table table, Player player) {
		if (checkDraw(player) == true)
			return true;
		else if (checkDiaganolLeft(player,table) == true)
			return true;
		else if (checkDiaganolRight(player,table) == true)
			return true;
		for (int i = 1; i < 4; i++) {
			if (checkRow(player,table, i) == true) {
				return true;
			} else if (checkColumn(player,table,i) == true) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkRow(Player player,Table table,int row) {
		if (table.getRowCol(row, 1).equals(table.getRowCol(row, 2)) && table.getRowCol(row, 1).equals(table.getRowCol(row, 3))
				&& !table.getRowCol(row, 1).equals("-") == true) {
			showWin(player,table,row);
			return true;
		}
		return false;
	}

	public static boolean checkColumn(Player player,Table table,int col) {
		if (table.getRowCol(1, col).equals(table.getRowCol(2, col)) && table.getRowCol(1, col).equals(table.getRowCol(3, col))
				&& !table.getRowCol(1, col).equals("-") == true) {
			showWin(player,table,col);
			return true;
		}
		return false;
	}

	private static boolean checkDiaganolLeft(Player player,Table table) {
		if (table.getRowCol(1, 1).equals(table.getRowCol(2,2)) && table.getRowCol(1, 1).equals(table.getRowCol(3, 3)) && !table.getRowCol(1, 1).equals("-") == true) {
			showWinDiaganolLeft(table);
			return true;
		}
		return false;
	}

	private static boolean checkDiaganolRight(Player player,Table table) {
		if (table.getRowCol(1, 3).equals(table.getRowCol(2,2)) && table.getRowCol(1, 3).equals(table.getRowCol(3, 1)) && !table.getRowCol(1, 3).equals("-") == true) {
			showWinDiaganolRight(table);
			return true;
		}
		return false;
	}

	public static boolean checkDraw(Player player) {
		if (player.getTurn() == 9) {
			System.out.println("Draw !!!");
			return true;
		}
		return false;
	}
	public static void showWin(Player player,Table table,int i) {
		table.getBoard();
		System.out.println(table.getRowCol(i, i) + " WIN !!!");
		showTextEnd();
	}

	public static void showWinDiaganolLeft(Table table) {
		table.getBoard();
		System.out.println(table.getRowCol(1, 1) + " WIN !!!");
		showTextEnd();
	}

	public static void showWinDiaganolRight(Table table) {
		table.getBoard();
		System.out.println(table.getRowCol(1, 3) + " WIN !!!");
		showTextEnd();
	}

}
