package oxOOP;

import java.util.Scanner;


public class main {
	public static void showTextAsk() {
		System.out.println();
		System.out.println("Play Again ? ");
		System.out.print("Y/N : ");
	}
	public static void checkAns(String ans,boolean checkPlayAgain) {
		if(ans.equalsIgnoreCase("Y") || ans.equalsIgnoreCase("Yes")) {
			checkPlayAgain=true;
		}else if(ans.equalsIgnoreCase("n") || ans.equalsIgnoreCase("no")) {
			checkPlayAgain=false;
		}else playAgain();
	}
	public static boolean playAgain() {
		Scanner sc = new Scanner(System.in);
		showTextAsk();
		String ans;
		boolean checkPlayAgain=false;
		while(true) {
			ans=sc.next();
			checkAns(ans,checkPlayAgain);
			if(checkPlayAgain==true) {
				return true;
			}return false;
		}
		
}
	public static void main(String[] args) {
		boolean again=true;
		Game game = new Game();
		while(again==true) {
			game.startGame();
			again=playAgain();
		}
		Game.showTextEnd();
	}

}
