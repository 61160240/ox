/**
 * 
 */
package oxOOP;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/**
 * @author ASUS
 *
 */
class UnitTestOX {

	@Test
    public void testShowWelcome(){
        Game g = new Game();
        g.showWelcome();
    }
	@Test
	public void testShowTextEnd(){
        Game g = new Game();
        g.showTextEnd();
    }
	@Test
    public void testGetBoard(){
        Table t = new Table();
        t.getBoard();
    }
	@Test
	public void testShowTurn() {
		Game g = new Game();
		g.showTurn(new Player());
	}
	@Test
	public void testShowTextInput() {
		Game g = new Game();
		g.showTextInput();
	}
	@Test
	public void testIsStringInteger() {
		Game g = new Game();
		assertEquals(true,g.isStringInteger("1", "2")) ;
	}
	@Test
	public void testCheckIndexOutOfBound() {
		Game g = new Game();
		assertEquals(true,g.checkIndexOutOfBound("1", "2"));
	}
	/*@Test
	public void testSetTurn() {
		Game g = new Game();
		g.setTurn(new Player(), new Table());
	}*/
	@Test
	public void testInput() {
		Game g = new Game();
		g.input(new Player(), "2", "2",new Table());
	}
	@Test
	public void testGetPlayer() {
		Player p = new Player();
		assertEquals("o",p.getPlayer());
	}
	@Test
	public void testGetRowCol() {
		Table t = new Table();
		t.getRowCol(1,1);
	}
}
