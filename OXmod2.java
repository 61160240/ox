import java.util.Scanner;

public class OXmod2 {
	public static void showWelcome() {
		System.out.println("Welcome to OX game");
	}

	public static void showTextInput() {
		System.out.print("Please put row and colume: ");
	}
	public static void showTextEnd() {
		System.out.print("Good luck Bye Bye !!");
	}
	public static void showTextAsk() {
		System.out.println("Play Again ? ");
		System.out.print("Y/N : ");
	}
	public static void setBoard(String board[][]) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i == 0) {
					if (j == 0) {
						board[i][j] = " ";
					} else {
						board[i][j] = String.valueOf(j);
					}
				} else {
					if (j == 0) {
						board[i][j] = String.valueOf(i);
					} else {
						board[i][j] = String.valueOf("-");
					}
				}
			}
		}
	}

	public static void showBoard(String board[][]) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(board[i][j]);
				System.out.print(" ");
			}
			System.out.println();
		}
	}

	public static boolean isStringInteger(String x, String y) {
		try {
			Integer.parseInt(x);
			Integer.parseInt(y);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	public static boolean checkIndexOutOfBound(String row,String col) {
		if (Integer.parseInt(row) > 3 || Integer.parseInt(col) > 3) {
			System.out.println("***Please put correct row and column !!!***");
			return false;
		}return true;
	}
	public static void checkInput(String[][] board, int turn) {
		String row,col;
		while (true) {
			Scanner sc = new Scanner(System.in);
			showTextInput();
			row = sc.next();
			col = sc.next();
			if (isStringInteger(row, col) == false) {
				System.out.println("****Please put correct row and column !!!****");
				continue;
			}
			if (checkIndexOutOfBound(row,col)==false) {
				continue;
			}else break;
		}
		input(board, row, col, turn);
	}

	public static void input(String[][] board, String row, String col, int turn) {
		if (turn % 2 == 1) {
			inputO(Integer.parseInt(row), Integer.parseInt(col), board, turn);
		} else if (turn % 2 == 0) {
			inputX(Integer.parseInt(row), Integer.parseInt(col), board, turn);
		}
	}

	public static void inputX(int row, int col, String[][] board, int turn) {
		if (board[row][col] == "-") {
			board[row][col] = "X";
		} else {
			System.out.println("Please put correct row and column.");
			checkInput(board, turn);
		}
	}

	public static void inputO(int row, int col, String[][] board, int turn) {
		if (board[row][col] == "-") {
			board[row][col] = "O";

		} else {
			System.out.println("Please put correct row and column.");
			checkInput(board, turn);
		}
	}

	public static void setTurn(String[][] board) {
		boolean end = false;
		int turn = 1;
		while (end == false) {
			if (turn % 2 == 1) {
				end = mainGame("O", turn,board);
			} else if (turn % 2 == 0) {
				end = mainGame("X", turn,board);
			}
			turn++;
		}startAgain(board);
	}

	public static boolean mainGame(String player, int turn, String[][] board) {
		showTurn(player);
		checkInput(board, turn);
		showBoard(board);
		return checkWin(board, turn);
	}

	public static void showWin(String[][] board, int i) {
		System.out.println(board[i][i] + " WIN !!!");
	}

	public static void showWinDiaganolLeft(String[][] board) {
		System.out.println(board[1][1] + " WIN !!!");
	}

	public static void showWinDiaganolRight(String[][] board) {
		System.out.println(board[1][3] + " WIN !!!");
	}

	public static boolean checkWin(String[][] board, int turn) {
		if (checkDraw(turn) == true)
			return true;
		else if (checkDiaganolLeft(board) == true)
			return true;
		else if (checkDiaganolRight(board) == true)
			return true;
		for (int i = 1; i < 4; i++) {
			if (checkRow(i, board) == true) {
				return true;
			} else if (checkColumn(i, board) == true) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkRow(int row, String[][] board) {
		if (board[row][1].equals(board[row][2]) && board[row][1].equals(board[row][3])
				&& !board[row][1].equals("-") == true) {
			showWin(board, row);
			return true;
		}
		return false;
	}

	public static boolean checkColumn(int col, String[][] board) {
		if (board[1][col].equals(board[2][col]) && board[1][col].equals(board[3][col])
				&& !board[1][col].equals("-") == true) {
			showWin(board, col);
			return true;
		}
		return false;
	}

	private static boolean checkDiaganolLeft(String[][] board) {
		if (board[1][1].equals(board[2][2]) && board[1][1].equals(board[3][3]) && !board[1][1].equals("-") == true) {
			showWinDiaganolLeft(board);
			return true;
		}
		return false;
	}

	private static boolean checkDiaganolRight(String[][] board) {
		if (board[1][3].equals(board[2][2]) && board[1][3].equals(board[3][1]) && !board[1][3].equals("-") == true) {
			showWinDiaganolRight(board);
			return true;
		}
		return false;
	}

	public static boolean checkDraw(int turn) {
		if (turn == 9) {
			System.out.println("Draw !!!");
			return true;
		}
		return false;
	}

	public static void showTurn(String player) {
		System.out.print(player.toUpperCase());
		System.out.println(" turn");
	}
	public static void startAgain(String[][] board) {
		showTextAsk();
		Scanner sc = new Scanner(System.in);
		String ans=sc.next();
		if(checkAns(ans)==true) {
			showWelcome();
			setBoard(board);
			showBoard(board);
			setTurn(board);
		}else showTextEnd();
	}
	public static boolean checkAns(String ans) {
		if(ans.equalsIgnoreCase("Y") || ans.equalsIgnoreCase("Yes")) {
			return true;
		}return false;
	}
	public static void main(String[] args) {
		String[][] board = new String[4][4];
		showWelcome();
		setBoard(board);
		showBoard(board);
		setTurn(board);
	}
}
