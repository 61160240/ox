
import java.util.InputMismatchException;
import java.util.Scanner;

public class OXmod {
    public static String [][] board = new String[4][4];
    public static int turn=1;
    public static boolean end=false;
    public static String row,col;
    public static Scanner sc = new Scanner(System.in);
    public static void showWelcome(){
        System.out.println("Welcome to OX game");
    }
    public static void setBoard(){
        for(int i = 0 ; i < 4 ; i++){
            for(int j = 0 ; j < 4 ; j++){
                if(i==0){
                    if(j==0){
                        board[i][j] = " ";
                    }else{
                        board[i][j] = String.valueOf(j);
                    }
                }else{
                    if(j==0){
                        board[i][j]= String.valueOf(i);
                    }else{
                        board[i][j]=String.valueOf("-");
                    }
                }
            }
        }
    }
    public static void showBoard(){
         for(int i = 0 ; i < 4 ; i++){
            for(int j = 0 ; j < 4 ; j++){
                System.out.print(board[i][j]);
                System.out.print(" ");
            }System.out.println();
        }
    }
    public static void showTurn(){
        if(turn%2==1){
            System.out.println("X turn");
        }
        else if(turn%2==0){
            System.out.println("O turn");
            
        }
    }
    public static boolean isStringInteger(String x,String y){
        try{
            Integer.parseInt(x);
            Integer.parseInt(y);
        }catch(Exception e ){
            return false;
        }
        return true;
    }
    public static void checkInput() {
    	while(true) {
    	row=sc.next();
    	col=sc.next();
    	if(isStringInteger(row,col)==false) {
    		System.out.println("****Please put correct row and column !!!****");
    		showTextInput();
    		continue;
    	}
    	if(Integer.parseInt(row)>3&&Integer.parseInt(col)>3) {
    		System.out.println("***Please put correct row and column !!!***");
    		showTextInput();
    	}
    	else break;
    	}input();
    }
    public static void input() {
    	
    	if(turn%2==0) {
        	inputO(Integer.parseInt(row),Integer.parseInt(col));
    	}
        else if(turn%2==1) {
        	inputX(Integer.parseInt(row),Integer.parseInt(col));
        }
    }
    public static void showTextInput() {
    	showTurn();
    	System.out.print("Please put row and colume: ");
    }
    public static void startGame(){
        while(end==false){
        turn++;
        showBoard();
        showTextInput();
        checkInput();
        checkWin();
        
        }
    }
    public static void inputX(int row,int col) {
    	if(board[row][col]=="-"){
            board[row][col] = "X";
        }else{
            turn--;
            System.out.println("Please put correct row and column.");
        }
    }
    public static void inputO(int row,int col) {
    	if(board[row][col]=="-"){
            board[row][col] = "O";
        }else{
            turn--;
            System.out.println("Please put correct row and column.");
        }
    }
    public static void checkWin(){
    	int countLeftToRight = 0 , countRightToLeft = 0;
        for(int i = 1 ; i < 4 ; i++){
        	int count_col = 0 , count_row =0;
        	countLeftToRight += checkDiaganol(i); countRightToLeft += checkDiaganol(i);
            for(int j = 1 ; j < 4;j++ ){
            	count_col += checkCol(i,j); count_row += checkRow(i,j);
            	winColumn(count_col,i);
            	winRow(count_row,i);}}
        winDiaganol(countLeftToRight);
        winDiaganol2(countRightToLeft);
        checkDraw();
    }
    private static void checkDraw() {
    	if((turn==9)&&(!end)){
            end = true;
            System.out.println("Darw !!!");
        }
    }
    private static int checkDiaganol(int i) {
    	if((board[1][1].equals(board[i][i])) && (!board[1][1].equals("-"))){
    		return 1;
        }
    	else if((board[1][3].equals(board[i][4-i])) && (!board[1][3].equals("-"))){
    		return 1;
    		}
    	return 0;
    }
    private static void winDiaganol(int x) {
    	if(x == 3) {
    		System.out.println(board[1][1]+" Win !!!");
    		System.out.println("bye bye ....");
    		end = true;
    	}
    }
    private static void winDiaganol2(int x) {
    	if(x == 3) {
    		System.out.println(board[1][3]+" Win !!!");
    		System.out.println("bye bye ....");
    		end = true;
    	}
    }
    private static int checkCol(int i, int j) {
    	if((board[1][i].equals(board[j][i]))&&(!board[1][i].equals("-"))){
    		return 1; 
         }
    	return 0;
    }
    private static void winColumn(int x,int i) {
    	if(x == 3) {
    		System.out.println(board[1][i]+" Win !!!");
    		System.out.println("bye bye ....");
    		end = true;
    	}
    }
    private static int checkRow(int i , int j) {
    	if((board[i][1].equals(board[i][j]))&&(!board[i][1].equals("-"))){
            return 1; 
         }
    	return 0;
    }
    private static void winRow(int x,int i) {
    	if(x == 3) {
    		System.out.println(board[i][1]+" Win");
    		System.out.println("bye bye ....");
    		end = true;
    	}
    }
    public static void main (String[] args){
        showWelcome();
        setBoard();
        startGame();
    }
}
